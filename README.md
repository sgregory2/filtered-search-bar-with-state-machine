# filtered search bar with state machine example

* tracks triplets of token keys
* supports editing current or previous tokens
* press Escape to restore from an edit

pros:
* managing state can allow for nuanced, controlled behavior.

cons:
* implementation is poor
* managing state can be brittle

## to view

`npm run dev`