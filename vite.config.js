import { defineConfig } from 'vite'
import { createVuePlugin } from "vite-plugin-vue2";
import process from 'process';

// https://vitejs.dev/config/
export default defineConfig({
  base: process.env.NODE_ENV === 'production' ? 'filtered-search-bar-with-state-machine' : '/',
  plugins: [createVuePlugin()]
})
